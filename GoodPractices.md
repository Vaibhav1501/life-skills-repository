# Good Practices for Software Development
### Which point(s) were new to you?
+ Get to know your teammates
+ Always over-communicate
+ Be aware and mindful of other team members

### Which area do you think you need to improve on? What are your ideas to make progress in that area?

+ Explaining the problem clearly, mention the solutions you tried out to fix the problem.
+  Implementation taking longer than usual due to some unexpected issue - Inform relevant team members
+  Asking questions and seek clarity in the meeting itself.