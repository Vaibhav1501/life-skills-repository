# Prevention of Sexual Harassment

## Questions:
1. ### What kinds of behaviour cause sexual harassment?
   
   * #### Unwanted Physical Contact:      
        This can include touching, groping, patting, or any other physical contact of a sexual nature without consent.

   * #### Sexual Comments or Jokes:  
        Making sexually suggestive comments, jokes, or innuendos, either in person, through written communication, or online, that create a hostile or uncomfortable environment.

   * #### Repeated Unwanted Advances:    
        Continuously asking someone out, making unwanted romantic or sexual advances despite clear indications of disinterest or rejection.

   * #### Unwanted Sexual Propositions:  
        Pressuring someone for sexual favors, suggesting sexual activity in exchange for something (like a promotion, grade, or job opportunity), or making threats if sexual advances are rejected.

   * #### Inappropriate Display of Sexual Material:   
        Showing explicit images, videos, or other materials of a sexual nature in the workplace or other environments where it's not appropriate.

   * #### Stalking or Persistent Following:       
        Continuously monitoring someone's activities, following them, or showing up uninvited in places where they are, which can create a sense of fear or intimidation.

   * #### Sexual Assault or Coercion: 
        Any form of non-consensual sexual contact or activity, including rape, attempted rape, or any coerced sexual activity.
2. ### What would you do in case you face or witness any incident or repeated incidents of such behaviour?
   * Document the incident.
   * Report it to the appropriate authority.
   * Seek support from trusted individuals.
   * Use available resources and policies.
   * Consider legal options if necessary.
   * Prioritize self-care.
   * Support others who may be experiencing harassment.