# Tiny Habits

#### Question 1:   The most interesting idea by BJ Fogg:
Tiny habits are very small, easy behaviors that you can start with to achieve significant behavior change.
The speaker, BJ Fogg, argues that these tiny habits are so easy that they require minimal motivation and willpower to perform. The key to this method is pairing the new behavior with an existing habit that you already perform regularly. For example, doing two push-ups after you use the restroom. By pairing the new behavior with an existing one, the existing behavior serves as a trigger or reminder to perform the new behavior.

#### Question 2 :   How to use B = MAP to make making new habits easier? What are M, A and P.
According to the video, Tiny Habits by BJ Fogg talks about a three-part method to develop new habits easily.


**Motivation (M)**: This refers to your desire or willingness to do something.

**Ability (A)**: This refers to your capacity to perform a behavior.

**Prompt (P)**: This refers to a cue or trigger that reminds you to do something.

The key is to make the habit small and achievable. The smaller the behavior, the less motivation you'll need and the more likely you are to stick with it.

The second part is to identify an action prompt. This means pairing your new habit with something you already do.

The third part is to celebrate your wins. Even if you only do a small thing, you should take a moment to feel proud of yourself. Celebrating small wins increases your motivation and makes you more likely to repeat the behavior.

#### Question 3:    Why it is important to "Shine" or Celebrate after each successful completion of habit?
Celebrating success after completing a habit reinforces the behavior in your brain. It's like saying "good job!" to yourself, which makes your brain want to do it again. So, celebrating after each success helps turn the behavior into a habit, this is how our mind work.

#### Question 4:    The most interesting story from 1% Better Every Day By James Clear
According to the video, the most interesting idea was about how changing our perspective can transform our experiences. This means that small improvements can lead to remarkable results over time. The story about the man who changed the board for the ship was really eye-opening. It showed how shifting our mindset can lead to big change over time.

#### Question 5:    Perspective about Identity from Atomic Habits Book by James Clear?

Dynamic Identity: The book suggests that identity is not fixed it constantly evolves over time.
Influenced by External Factors: It emphasizes how identity is shaped by external influences such as culture, society, and relationships.
Intersectionality: It explores how different aspects of identity, like race, gender, and class, intersect and affect each other.
Complexity: Identity is portrayed as fluid and complex, challenging the idea of simple categorization.


#### Question 6:    Book's perspective on how to make a habit easier to do?

Start Tiny: Begin with a tiny, manageable action related to the habit you want to develop.4

Stay Consistent: Regularly practice the habit, even in small increments, to integrate it into your routine effectively.

Reflect and Learn: Take time to reflect on your progress and how the habit impacts your life, reinforcing its significance.

Enjoy small wins: Celebrate even the smallest victories along your habit-building journey to maintain motivation.


#### Question 7:    Book's perspective on how to make a habit harder to do?


+ Increase Difficulty
+ Change Environment
+ Delay It
+ Negative Consequences



#### Question 8:    Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

Habit: Reading more books

+ Make it accessible.
+ Make the Habit More Attractive
+ Make the Habit Easier
+ Celebrate


#### Question 9:    Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

Habit: Spending too much time on social media
+ Make it unaccessible
+ Make it Unattractive
+ Make the Habit Harder
+ Shift the focus