# Learning Process

## Questions and answere:
1. ### What is the Feynman Technique? Explain in 1 line.
    Explaining a concept in simple terms as if teaching it to someone else.

2. ### In this video, what was the most interesting story or idea for you?
    Holding key and relaxing letting thoughts do their work until just about to sleep and when you are about to sleep falling of keys wakes you up and put all those ideas in brains in solving the problem.

3. ### What are active and diffused modes of thinking?

    Think of the active mode as when we're laser-focused on a task, and the diffused mode as when our mind is wandering or daydreaming, allowing for more creative thinking.

4. ### According to the video, what are the steps to take when approaching a new topic? Only mention the points.
+ Diconstruct the skill.
+ Learn enough to self-correct.
+ Remove practice barriers.
+ Practice atleast 20 hours.
  
5. ### What are some of the actions you can take going forward to improve your learning process?
+ focus on learnig enough to get started rather than focusing on mastering.