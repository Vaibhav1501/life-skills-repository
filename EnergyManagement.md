# Energy Mangement

### What are the activities you do that make you relax - Calm quadrant?
 - To relax in the calm quadrant, I enjoy activities like playing FPS games, taking a walk outside, meditating, or simply listening to some calming music. These things help me unwind and feel refreshed.

### When do you find getting into the Stress quadrant?
- I usually end up in the stress quadrant when I have tight deadlines, tough tasks, or when things don't go as planned. But a little stress can sometimes motivate me to get things done.

### How do you understand if you are in the Excitement quadrant?

- Feeling eager and enthusiastic.
- Positive anticipation.
- Increased energy levels.
- Sense of adventure.


### Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

+ Sleep is crucial for our health and performance.
+ Lack of sleep affects our brain function, making it harder to concentrate and remember things.
+ During sleep, our brains consolidate memories and clean out toxins.
+ Lack of sleep increases the risk of different health issues, including heart disease and obesity.
+ A good sleep habit involves a consistent sleep schedule and creating a comfortable sleep environment.


### What are some ideas that you can implement to sleep better?
Here are some simple ideas to help sleep better:


+ Maintain regular sleep time. One need to go to bed and wake up at the time fixed every day.


+ Make sure bedroom is comfortable. Keep it cool, dark, and quiet for better sleep.


+ Avoid screens like phones or computers before bed, as the light can make it harder to fall asleep.


+ Don't eat heavy meals or drink caffeine close to bedtime, as they can keep awake.
  
+ Limit naps during the day, especially in the late afternoon or evening.



### Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.
The five key points are:
+ Doing regular exercise is really good for your brain.
+ When you exercise, your brain grows new cells, which makes you smarter.
+ It also makes you feel happier by releasing chemicals that make you feel good.
+ Exercise helps keep your brain healthy as you get older, lowering the chances of memory problems.
+ Even a little bit of exercise, like going for a walk, can make a big difference for your brain.


### What are some steps you can take to exercise more?


+ Start with small steps: Begin with short activities like a quick walk or stretching at home.


+ Choose enjoyable activities: Pick exercises that i like, whether walking, dancing, or playing sports.
